.. _seguridad:

Seguridad
=========

este módulo es súper importe desde controlamos los niveles de seguridad de nuestros sistema B365 por ejemplo los permisos que va tener un usuario administrador, cajero, root, etc. mejor dicho desde aquí gestionamos el rol que va tener cada persona en nuestra empresa, también podemos crear un tipo de perfil y asignar que permiso va tener y cuáles no.

Por ultimo pero no menos importa la asignación de esos usuarios a cada una nuestra seculares o unidades organizativas y por supuesto la creación también de sucursales.
Creación de usuarios. 

Creación de usuarios
--------------------
Pasos para crear un usuario

*1: Boton :guilabel:`Nuevo`.

*2: En el campo persona digite el nombre de la persona si lo sabe, sino esta seguro del nombre puede darle click en la lupa le mostra un menu de busqueda con distintos filtros podra buscar por email, numero de cedula entre otro tipos de filtro.

*3: Dale click el boton :guilabel:`Buscar`, iniciara la busqueda cuando encuentres el registro cierra la ventana dale click en la :guilabel:`x`.

*4: El campo email digita el correo electronico del usuario.

*5: En el campo contraseña digita la contraña que va tener el usuario.

*6 por ultimo click en el boton :guilabel:`Guardar`,

        .. seealso::

			link: https://erpv3.source-solutions.co/security/usuario/

Ruta::file:`Seguridad> Usuarios>`

Asignación del grupos o perfiles al usuario
-------------------------------------------

Crear Grupo
-----------
Un Grupo: es un divisor entre grupos de personas.

*1: Boton :guilabel:`Nuevo`.

*2: En el campo Nombre escribes el va tener tu grupo (*Obligatorio).


*3: Descripcion: este campo no es (*Obligatorio) pero puedes escribir una si deseas.

*4: con este numero o combinacion de letras y numeros podras identificar tu grupo es (*Obligatorio).

*5: Privado: (casilla de verificacion o checkbox) solo un click en el cuadrito si quiere tener un grupo privado.

*6 Activo: (casilla de verificacion o checkbox) le das click en el cuadrito si quiere tener el grupo activo.

*7 publicacion: ?

*8 por ultimo click en el boton :guilabel:`Guardar`.

Perfiles de Usuario:
--------------------
un perfil de usuario es entorno personalizado de configuración para un individuo.

*1: Boton :guilabel:`Nuevo`.

*2: Campo Grupo digite el nombre del grupo o dale click en la lupa para hacer una busqueda avanzada aqui puede hacer una busqueda avanzada con varios tipos de filtros entre algunos de ellos el identificador, privado, activo para iniciar la busqueda click en  :guilabel:`Buscar`. click en la casilla con el chulo.


*3: Escriba el nombre de la persona que va agregar al nuevo grupo o inicie una busqueda avanzada dando click en la lupa donde puede buscar por filtros avanzados como  nombre y correo electronico, para iniciar la busqueda click en  :guilabel:`Buscar`. click en la casilla con el chulo, cuando encuentres el registro cierra la ventana  click en la :guilabel:`x`.


*4: por ultimo click en el boton :guilabel:`Guardar`.



.. seealso::

    ` <https://erpv3.source-solutions.co/security/grupo-usuario>`_


Asignación de usuarios a sucursales
-----------------------------------

Unidad organizativa: es una estructura jerárquica con diferente forma por su clasificación 
Ejemplo: Corporativo: Administración> Gestión> suministros>Legal> Recursos Humanos.

Paso a Paso

*1: Boton :guilabel:`Nuevo`.

*2 Campo _Unidad_ Organizacional_ escribe el nombre de la *Unidad organizativa* o dale click en la lupa en este caso se mostrara una lista en forma de arbol estrutural con las sedes o unidades organizativas en la casilla con el chulo cuando encuentres el registro cierra la ventana  click en la :guilabel:`x`.


*3 Campo _Persona_ Escriba el nombre de la persona que va agregar al nuevo grupo o inicie una busqueda avanzada dando click en la lupa donde puede buscar por filtros avanzados como nombre, correo electronico y numero de cedula para iniciar la busqueda click en  :guilabel:`Buscar`. click en la casilla con el chulo, cuando encuentres el registro cierra la ventana  click en la :guilabel:`x`.

*4 por ultimo click en el boton :guilabel:`Guardar`.


.. seealso::

    ` <https://erpv3.source-solutions.co/security/persona-unidad/list>`_

`enviar mensaje <mailto:seguridad@source-solution.cot>`_